import json


def parse_json(file_name):
    with open((file_name), 'r', encoding='utf-8') as f:
        text = json.load(f)
    return text


def read_recipe():
    cook_book = parse_json('recipes.json')
    return cook_book


def print_recipes():
    cook_book = read_recipe()
    for key, value in cook_book.items():
        print_str = "Название блюда: {}, ".format(key)
        print_str += "кол-во ингридиентов: {}, ".format(value['ingridients_qty'])
        ingridients_list = value['ingridients']
        print_str += "список ингридиентов: "
        for ingr in ingridients_list:
            ingridient_name = ingr['ingridient_name']
            quantity = ingr['quantity']
            measure = ingr['measure']
            print_str += "{} - {} {}, " \
                .format(ingridient_name, quantity, measure)
        print(print_str)


def get_shop_list_by_dishes(dishes, person_count):
    shop_list = {}
    cook_book = read_recipe()
    for dish in dishes:
        if dish in cook_book:
            dish_details = cook_book[dish]
            for ingridient in dish_details['ingridients']:
                new_shop_list_item = ingridient
                dict((k, int(v)) for k, v in new_shop_list_item.items() if 'quantity' == k)
                new_shop_list_item['quantity'] *= person_count
                if new_shop_list_item['ingridient_name'] not in shop_list:
                    shop_list[new_shop_list_item['ingridient_name']] = new_shop_list_item
                else:
                    shop_list[new_shop_list_item['ingridient_name']]['quantity'] += new_shop_list_item['quantity']
    return shop_list


def print_shop_list(shop_list):
    for shop_list_item in shop_list.values():
        print('{} {} {}'.format(shop_list_item['ingridient_name'], shop_list_item['quantity'],
                                shop_list_item['measure']))


def create_shop_list():
    person_count = int(input('Введите количество человек: '))
    dishes = input('Введите блюда в расчете на одного человека (через запятую): ') \
        .lower().split(',')
    shop_list = get_shop_list_by_dishes(dishes, person_count)
    print_shop_list(shop_list)


print_recipes()
create_shop_list()
