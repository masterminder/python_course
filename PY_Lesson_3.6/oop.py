class Animal:
    count = 0

    def eat(self):
        pass

    def make_sound(self):
        pass

    def walk(self):
        pass


class Livestock(Animal): # крупный рогатый скот
    milk_per_day = 0

    def get_milk(self):
        pass

    def graze(self):
        pass


class Cow(Livestock):
    hay_amount_per_day = 0.0


class Goat(Livestock):

    def jump(self):
        pass


class Sheep(Livestock):
    wool_per_week = 0.0

    def get_wool(self):
        pass


class Bird(Animal):

    def flyblow(self): # откладывать яйца
        pass


class Goose(Bird):

    def nip(self): # щипать
        pass


class Chicken(Bird):

    def peck(self): # клевать
        pass


class Duck(Bird):

    def swim(self):
        pass







