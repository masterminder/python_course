def read_recipe():
      cook_book = {}
      with open('recipes.txt', 'r') as f:
            for line in f:
                  if not line.strip():
                        continue
                  dish_name = line.strip()
                  ingridients_quantity = int((f.readline()).strip())
                  ingridients_list = []
                  for i in range(ingridients_quantity):
                        ingridients_map = {}
                        ingridient = (f.readline()).strip()
                        ingridients = ingridient.split('|')
                        ingridients_map['ingridient_name'] = ingridients[0]
                        ingridients_map['amount'] = int(ingridients[1])
                        ingridients_map['measure'] = ingridients[2]
                        ingridients_list.append(ingridients_map)
                  cook_book[dish_name] = (ingridients_quantity, ingridients_list)
      return cook_book

def print_recipes():
      cook_book = read_recipe()
      for k,v in cook_book.items():
            ingridients_qty, ingridients_list = v
            print('Dish: %s,\n Quantity of ingridients: %s,\n List of ingridients: %s' % (
                  k, ingridients_qty, ingridients_list))

def get_shop_list_by_dishes(dishes, person_count):
      shop_list = {}
      cook_book = read_recipe()
      for dish in dishes:
            if dish in cook_book:
                        __, ingridients_list = cook_book[dish]
                        for ingridient in ingridients_list:
                              new_shop_list_item = ingridient
                              new_shop_list_item['amount'] *= person_count
                              if new_shop_list_item['ingridient_name'] not in shop_list:
                                    shop_list[new_shop_list_item['ingridient_name']] = new_shop_list_item
                              else:
                                    shop_list[new_shop_list_item['ingridient_name']]['amount'] += new_shop_list_item['amount']
      print(shop_list, '\n')
      return shop_list

def print_shop_list(shop_list):
      for shop_list_item in shop_list.values():
            print('{} {} {}'.format(shop_list_item['ingridient_name'], shop_list_item['amount'],
                                    shop_list_item['measure']))

def create_shop_list():
      person_count = int(input('Введите количество человек: '))
      dishes = input('Введите блюда в расчете на одного человека (через запятую): ') \
            .lower().split(',')
      shop_list = get_shop_list_by_dishes(dishes, person_count)
      print_shop_list(shop_list)

print_recipes()
create_shop_list()