from pprint import pprint

import requests

AUTHORIZE_URL = 'https://oauth.yandex.ru/authorize'
APP_ID = 'd4276306a3114c64919ddb4c18fb56f2'

auth_data = {
    'response_type': 'token',
    'client_id': APP_ID
}

AUTH_TOKEN = 'AQAAAAAIOMwXAARnMKrIOxmCmUmhnHwnjhEEWNY'


class MetricaBase:
    API_MANAGEMENT_URL = 'https://api-metrika.yandex.ru/management/v1/'
    API_STAT_URL = 'https://api-metrika.yandex.ru/stat/v1/'
    token = None

    def __init__(self, token):
        self.token = token

    def _get_headers(self):
        return {
            'Authorization': 'OAuth {}'.format(self.token),
            'Content-Type': 'application-json'
        }


class YandexMetrica(MetricaBase):

    def get_counters(self):
        headers = self._get_headers()

        r = requests.get(self.API_MANAGEMENT_URL + 'counters', headers=headers)
        return [Counter(self.token, counter['id']) for counter in r.json()['counters']]


class Counter(MetricaBase):
    def __init__(self, token, counter_id):
        self.counter_id = counter_id
        super().__init__(token)

    def get_visits(self):
        headers = self._get_headers()

        params = {
            'id': self.counter_id,
            'metrics': 'ym:s:visits'
        }

        r = requests.get(self.API_STAT_URL + 'data', params, headers=headers)
        pprint(r.content)
        return r.json()['data'][0]['metrics'][0]

    def get_views(self):
        headers = self._get_headers()

        params = {
            'id': self.counter_id,
            'metrics': 'ym:s:pageviews'
        }

        r = requests.get(self.API_STAT_URL + 'data', params, headers=headers)
        return r.json()['data'][0]['metrics'][0]

    def get_users(self):
        headers = self._get_headers()

        params = {
            'id': self.counter_id,
            'metrics': 'ym:s:users'
        }

        r = requests.get(self.API_STAT_URL + 'data', params, headers=headers)
        return r.json()['data'][0]['metrics'][0]



metrica = YandexMetrica(AUTH_TOKEN)
counters = metrica.get_counters()

for counter in counters:
    pprint(counter.get_visits())
    pprint(counter.get_users())
    pprint(counter.get_views())