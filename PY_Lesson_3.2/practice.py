import requests
import os


def translate_it(source_file, target_folder, source_lang, target_lang='ru'):
    """
        YANDEX translation plugin

        docs: https://tech.yandex.ru/translate/doc/dg/reference/translate-docpage/

        https://translate.yandex.net/api/v1.5/tr.json/translate ?
        key=<API-ключ>
         & text=<переводимый текст>
         & lang=<направление перевода>
         & [format=<формат текста>]
         & [options=<опции перевода>]
         & [callback=<имя callback-функции>]

        :param text: <str> text for translation.
        :return: <str> translated text.
        """
    url = 'https://translate.yandex.net/api/v1.5/tr.json/translate'
    key = 'trnsl.1.1.20170620T153815Z.5197f7b10ce81fc3.cdfd3f9215eca80cf599f1ba09436c2f6f0f5f00'

    lang = "{}-{}".format(source_lang, target_lang)

    current_path = os.path.abspath(os.path.dirname(__file__))
    path_to_source_file = os.path.join(current_path, source_file)
    path_to_target_folder = os.path.join(current_path, target_folder)
    path_to_target_file = os.path.join(path_to_target_folder, source_file)

    if not os.path.exists(target_folder):
        os.mkdir(target_folder)

    with open(path_to_source_file, 'r', encoding='utf-8') as f:
        text = f.read()

    params = {
        'key': key,
        'lang': lang,
        'text': text
    }

    response = requests.get(url, params=params).json()
    translation_text = response.get('text')[0]

    with open(path_to_target_file, 'w') as f:
        f.write(translation_text)


source_file = 'DE.txt'
source_lang = 'de'
target_folder = 'Translation'

translate_it(source_file, target_folder, source_lang)
