import os.path

migrations = 'Migrations'
current_path = os.path.abspath(os.path.dirname(__file__))


def get_sql_files():
    path_to_source = os.path.join(current_path, migrations)
    files = [os.path.join(path_to_source, f) for f in os.listdir(path_to_source) if f.endswith('.sql')]
    return files


def print_search_results(files):
    print('Список найденных файлов')
    for file in files:
        print(os.path.basename(file))
    print('Количество найденных файлов: {} '.format(len(files)))


def filter_files(files):
    print('Введите поисковый запрос: ')
    pattern = input()
    filtered_list = []
    for file in files:
        with open(file, 'r') as f:
            text = f.read()
            if pattern in text:
                filtered_list.append(file)
    print_search_results(filtered_list)
    if filtered_list:
        filter_files(filtered_list)
    else:
        print('Не было найдено ни одного файла, соответсвующего поисковому запросу!')

files_list = get_sql_files()
filter_files(files_list)
