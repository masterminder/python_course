import subprocess
import os

current_path = os.path.abspath(os.path.dirname(__file__))
path_to_source = os.path.join(current_path, 'Source')
path_to_target = os.path.join(current_path, 'Target')

def convert_image(image):
    if (not os.path.exists(path_to_target)):
        os.mkdir(path_to_target)
    abs_path_source = os.path.join(path_to_source, image)
    abs_path_target = os.path.join(path_to_target, image)
    args = "convert.exe {0} -resize 200 {1}".format(abs_path_source, abs_path_target)
    subprocess.call(args, shell=False)
    print("Image {0} was converted successfully. "
          "You can find resized image in the folder {1}".format(image, path_to_target))

images = [f for f in os.listdir(path_to_source)]
for image in images:
    convert_image(image)
