import requests
import urllib


class VkBase:
    BASE_API_URL = 'https://api.vk.com/method'
    VERSION = '5.62'
    token = None

    def __init__(self, token):
        self.token = token

    def send_request(self, method_name, params, request_type='GET'):

        default_params = {
            'access_token': self.token,
            'v': self.VERSION
        }

        params.update(default_params)

        if request_type is 'POST':
            r = requests.post(self.BASE_API_URL + method_name, data=params).json()
        else:
            r = requests.get(self.BASE_API_URL + method_name, params).json()

        if 'error' in r.keys():
            raise Exception('Error message: %s. Error code: %s' % (r['error']['error_msg'], r['error']['error_code']))

        return r['response']


class Groups(VkBase):
    def get_groups_list(self, user_id):
        params = {
            'user_id': user_id,
            'extended': 1,
            'fields': 'members_count',
        }

        response_data = self.send_request('/groups.get', params)
        return response_data['items']

    def is_member(self, group_id, user_ids):
        params = {
            'access_token': self.token,
            'group_id': group_id,
            'user_ids': user_ids,
            'v': self.VERSION
        }

        response_data = self.send_request('/groups.isMember', params, 'POST')
        return response_data

    def get_members(self, group_id):
        params = {
            'access_token': self.token,
            'group_id': group_id,
            'filter': 'friends',
            'v': self.VERSION
        }

        response_data = self.send_request('/groups.getMembers', params)
        return response_data['items']


class Friends(VkBase):
    def get_friends_list(self, user_id):
        params = {
            'access_token': self.token,
            'user_id': user_id,
            'v': self.VERSION
        }

        response_data = self.send_request('/friends.get', params)
        return response_data['items']
