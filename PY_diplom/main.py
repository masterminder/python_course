from base import Groups, Friends
import json
import time

start_time = time.time()

AUTH_TOKEN = '5dfd6b0dee902310df772082421968f4c06443abecbc082a8440cb18910a56daca73ac8d04b25154a1128'
USER_ID = '5030613'

groups = Groups(AUTH_TOKEN)
groups_list = groups.get_groups_list(USER_ID)

friends = Friends(AUTH_TOKEN)
friends_list = friends.get_friends_list(USER_ID)

target_groups = []
seen_groups = set()


def split_list(list, chunk_size):
    return [list[offs:offs + chunk_size] for offs in range(0, len(list), chunk_size)]


for group in groups_list:
    for chunk in split_list(friends_list, 500):
        source_ids = ', '.join([str(x) for x in chunk])
        group_members = groups.is_member(group['id'], source_ids)
        in_group = any(d['member'] == 1 for d in group_members)
        if group['id'] not in seen_groups and in_group is False:
            required_fields = ['name', 'id', 'members_count']
            group_sh = {key: value for key, value in group.items() if key in required_fields}
            target_groups.append(group_sh)
            seen_groups.add(group['id'])
            break
        else:
            continue

with open('groups.json', 'w+', encoding='utf-8') as fp:
    json.dump(target_groups, fp, ensure_ascii=False)

print("--- %s seconds ---" % (time.time() - start_time))
