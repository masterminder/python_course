# Задача №3
import osa
import re

URL = "http://www.webservicex.net/length.asmx?WSDL"

client = osa.client.Client(URL)


def convert_dist(path):
    with open(path, 'r') as f:
        text = f.read()
        temps = text.split('\n')
        filtered = [float(re.sub("[^0-9.]", "", d)) for d in temps]
        result = sum(filtered)

    response = client.service.ChangeLengthUnit(LengthValue=result,
                                               fromLengthUnit='Miles',
                                               toLengthUnit='Kilometers')

    print('The total distance is equal to {0} kilometers'.format(round(response, 2)))


convert_dist('travel.txt')
