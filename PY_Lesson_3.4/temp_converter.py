#Задача №1
import osa
import re

URL = "http://www.webservicex.net/ConvertTemperature.asmx?WSDL"

client = osa.client.Client(URL)

def convert_temp(path):
    with open(path, 'r') as f:
        text = f.read()
        temps = text.split('\n')
        filtered = [int(re.sub("\D", "", d)) for d in temps]
        result = sum(filtered) / len(filtered)

    response = client.service.ConvertTemp(Temperature = result,
                               FromUnit = 'degreeFahrenheit',
                               ToUnit = 'degreeCelsius')

    print('The average temperature for last 7 days is {} by Celsius'.format(round(response,1)))

convert_temp('temps.txt')