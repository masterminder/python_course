#Задача №3
import osa

URL = "http://fx.currencysystem.com/webservices/CurrencyServer4.asmx?WSDL"

client = osa.client.Client(URL)
default_currency = 'RUB'

def convert_currency(path):
    with open(path, 'r') as f:
        text = f.read()
        temps = text.split('\n')
        filtered = [(d.split(':')[1]) for d in temps]
        total = 0
        for cur in filtered:
            amount = float(cur.split()[0].strip())
            currency_code = cur.split()[1].strip()

            convertion_amount = client.service.ConvertToNum(fromCurrency=currency_code,
                                                            toCurrency=default_currency,
                                                            amount=amount,
                                                            rounding=True)
            total += convertion_amount

    print('The total cost of trip is equal to {} {}'.format(total, default_currency))

convert_currency('currencies.txt')